import {useState, useEffect, useRef} from "react";
import './App.css';
import axios from "axios";
import { Liquid } from "liquidjs";

function App() {
  const [ps, setPs] = useState([]);
  const [doc, setDoc] = useState("");

  const [showEditor, setShowEditor] = useState(false);
  const [firstValue, setFirstValue] = useState("first");
  const [secondValue, setSecondValue] = useState("second");

  const contentRef = useRef(null);

  useEffect(()=> {
    const handleWindowMessage = (e) => {
      console.log("JWT received " + e.data);
      axios.defaults.headers.common["Authorization"] = "Bearer " + e.data;
    }

    window.addEventListener("message", handleWindowMessage);

    return () => {
      window.removeEventListener("message", handleWindowMessage);
    }
  }, []);

  const fetchProducts = () => {
    axios.get("https://api.dev.monotik.straxus.hu/api/seller/products").then(res => setPs(res.data._embedded.items), err => alert("err "+JSON.stringify(err.response.data)));
  }

  const renderDoc = () => {
    const engine = new Liquid();

    const template = `<!doctype html>
    <html>
        <head>
            <title>{{title}}</title>
        </head>
    
        <body>
        <div class="template-section">{{first}}</div>
        <div class="template-section">{{second}}</div>
        </body>
    </html>
    `;

    engine.parseAndRender(template, {title: "Hello world", first: firstValue, second: secondValue}).then(content => {
      setDoc(content);

      const style = `.template-section:hover {
        background-color: red;
        cursor: pointer;
      }`;
  
      const styleNode = contentRef.current.contentDocument.createElement("style");
  
      styleNode.type = "text/css";
      styleNode.appendChild(document.createTextNode(style));
  
      contentRef.current.contentDocument.head.appendChild(styleNode);

      const elements = contentRef.current.contentDocument.getElementsByClassName("template-section");

      for(let i = 0; i < elements.length; i++) {
        elements[i].addEventListener("click", () => setShowEditor(true));
      }
    });
  }

  const acceptButtonClicked = () => {
    renderDoc();
    setShowEditor(false);
  }

  return (
    <div className="App">
        <button onClick={renderDoc}>Render doc!</button>
        <br/>
        {
          showEditor && <>
            <input value={firstValue} onChange={e => setFirstValue(e.target.value)}/>
            <input value={secondValue} onChange={e => setSecondValue(e.target.value)}/>
            <button onClick={acceptButtonClicked}>Accept</button>
            <br/>
          </>
        }
        <iframe className="render-content" srcDoc={doc} ref={contentRef} />
        <br/>
        <button onClick={fetchProducts}>Fetch products</button>
        <ul>
        {
          ps.map(p => <li>{p.name}</li>)
        }
        </ul>
    </div>
  );
}

export default App;
